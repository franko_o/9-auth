<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/auth/facebook', 'SocialAuthController@redirectToProvider');
Route::get('/auth/facebook/callback', 'SocialAuthController@handleProviderCallback');

Route::get('/home', 'HomeController@index')->name('home');

Route::middleware(['auth'])->group(function () {

    Route::get('/currencies', 'CurrencyController@index')->name('currencies_list');
    Route::get('/currencies/add', 'CurrencyController@add')->name('add_currency');

    Route::put('/currencies/{id?}', 'CurrencyController@store');
    Route::post('/currencies/{id?}', 'CurrencyController@store')->name('store_currency');

    Route::get('/currencies/{id}', 'CurrencyController@show');

    Route::get('/currencies/{id}/edit', 'CurrencyController@edit');

    Route::delete('/currencies/{id}/destroy', 'CurrencyController@destroy')->name('delete_currency');
    Route::delete('/currencies/{id}', 'CurrencyController@destroy');
});


