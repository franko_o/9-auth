<?php

use Faker\Generator as Faker;


$factory->define(App\Currency::class, function (Faker $faker) {
    return [
        'title' => $faker->name,
        'short_name' => str_random(4),
        'logo_url' => $faker->url,
        'price' => $faker->randomFloat(4, 0, 10000)
    ];
});