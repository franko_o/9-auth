<?php

namespace App\Http\Controllers;

use App\Http\Requests\CurrencyRequest;
use App\Currency;
use Illuminate\Support\Facades\Auth;

class CurrencyController extends Controller
{
    public function index()
    {
        $currencies = Currency::all();
        $title = 'Currency market';
        if ($currencies->count() == 0) {
            return view('nocurrencies', compact('title'));
        }
        return view('currencies', compact('title', 'currencies'));
    }

    public function show($id)
    {
        $currencies = Currency::all();

        $currency = Currency::find($id);
        if (!$currency) {
            abort(404);
        }
        $title = $currency->title;

        return view('currency.view', compact('title', 'currency', 'currencies'));
    }

    public function add()
    {
        if (! Auth::user()->can('create', Currency::class)) {
            return redirect('/');
        }

        $currencies = Currency::all();
        $title = 'Add currency';
        return view('currency.new', compact('title', 'currencies'));
    }

    public function edit($id)
    {
        $currency = Currency::find($id);

        if (! Auth::user()->can('update', $currency)) {
            return redirect('/');
        }

        if (! $currency) {
            abort(404);
        }
        $title = $currency->title;

        $currencies = Currency::all();
        return view('currency.edit', compact('title', 'currency', 'currencies'));
    }

    public function store(CurrencyRequest $request, $id = null)
    {
        $isEdit = $id !== null;
        $model = $isEdit ? Currency::findOrFail($request->id) : new Currency();

        if (! Auth::user()->can('update', $model)) {
            return redirect('/');
        }

        $model->title = $request->title;
        $model->short_name = $request->short_name;
        $model->logo_url = $request->logo_url;
        $model->price = $request->price;
        $model->save();

        return $isEdit ? redirect('/currencies/' . $model->id) : redirect('/currencies/');
    }

    public function destroy($id)
    {
        $model = Currency::findOrFail($id);
        if (! Auth::user()->can('delete', $model)) {
            return redirect('/');
        }

        $model = Currency::findOrFail($id);
        $model->delete();
        return redirect('currencies');
    }
}
