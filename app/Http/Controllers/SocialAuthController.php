<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;

class SocialAuthController extends Controller
{
    protected $redirectPath = '/currencies';

    public function redirectToProvider(Request $request)
    {
        return Socialite::driver('facebook')->redirect();
    }

    public function handleProviderCallback()
    {
        $socialUser = Socialite::driver('facebook')->user();

        $user = User::where(['email' => $socialUser->email])->first();

        if (is_null($user)) {
            $user = User::create([
                'name' => $socialUser->name,
                'email' => $socialUser->email,
                'password' => md5(rand(1,10000))
            ]);
        }

        Auth::login($user);

        return redirect($this->redirectPath);
    }
}