<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Rules\Decimal;

class CurrencyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Override messages
     *
     * @return array
     */
    /*public function messages()
    {
        return [
            'logo_url.url' => "Don't panic. Just fill out a valid url",
            'min' => 'The :attribute is too small'
        ];
    }*/

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => 'integer',
            'title' => 'required|string|max:255',
            'short_name' => 'bail|required|string|min:2|max:10',
            'logo_url' => 'required|string|url',
            'price' => 'required|numeric|min:0'
        ];
    }
}
