@extends('layouts.frontend')

@section('header')
    @include('parts.header')
@stop

@section('content')

    <h2 class="content-title">Currency {{$currency->title}} Information</h2>

    <div class="row currency_info">
        <div class="col-sm-1"><img src="{{$currency->logo_url}}"></div>
        <div class="col-sm-3">{{$currency->title}}</div>
        <div class="col-sm-1 short">{{$currency->short_name}}</div>
        <div class="col-sm-7">{{$currency->price}} USD</div>
    </div>

    <div class="actions">
        @include('currency.actions')
    </div>

    @include('additional.backtolist')
@stop

@section('footer')
    @include('parts.footer')
@stop